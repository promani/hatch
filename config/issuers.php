<?php
/**
 * You should extend this file with your owns issuers.
 */
return [
    'default' => ['type' => 'RSA', 'alg' => 'RS256', 'public' => 'file://config/keys/default.pem'],
];
