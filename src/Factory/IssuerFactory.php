<?php
namespace App\Factory;

use App\Entity\Issuer;
use App\Exception\UnknownIssuerException;

class IssuerFactory
{
    /**
     * @var array
     */
    private $issuers;

    /**
     * Gatekeeper constructor.
     */
    public function __construct()
    {
        $this->issuers = include dirname(__DIR__).'/../config/issuers.php'; //Se podría ir a buscar al SSO y guardarse en la cahe
    }

    /** @throws UnknownIssuerException */
    public function create(string $name): Issuer
    {
        if (!array_key_exists($name, $this->issuers)) {
	        $name = 'default';
        }
        $configs = $this->issuers[$name];

        return new Issuer($configs['type'], $configs['alg'], $configs['public']);
    }

}
