<?php
namespace App\Entity;

use Lcobucci\JWT\Signer\Rsa;
use Lcobucci\JWT\Signer\Hmac;
use Lcobucci\JWT\Signer\Ecdsa;

class Issuer
{
    private string $type;
    private string $algorithm;
    private string $public;

    /**
     * Issuer constructor.
     */
    public function __construct(string $type, string $algorithm, string $public)
    {
        $this->type = $type;
        $this->algorithm = $algorithm;
        $this->public = $public;
    }

    public function getPublic() {
        return $this->public;
    }

    public function getSigner() {
        switch ($this->type) {
            case 'RSA':
                switch ($this->algorithm) {
                    case 'RS384':
                        return new Rsa\Sha384();
                    case 'RS512':
                        return new Rsa\Sha512();
                    default:
                        return new Rsa\Sha256();
                }
            case 'ECDSA':
                switch ($this->algorithm) {
                    case 'RS384':
                        return new Ecdsa\Sha384();
                    case 'RS512':
                        return new Ecdsa\Sha512();
                    default:
                        return new Ecdsa\Sha256();
                }
            default:
                switch ($this->algorithm) {
                    case 'RS384':
                        return new Hmac\Sha384();
                    case 'RS512':
                        return new Hmac\Sha512();
                    default:
                        return new Hmac\Sha256();
                }
        }
    }

}