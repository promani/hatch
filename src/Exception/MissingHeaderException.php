<?php
namespace App\Exception;

use Slim\Exception\HttpBadRequestException;

class MissingHeaderException extends HttpBadRequestException
{
    protected $title = 'Missing header in the request.';
}