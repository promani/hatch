<?php
namespace App\Exception;

use Slim\Exception\HttpBadRequestException;

class InvalidTokenException extends HttpBadRequestException
{
    protected $title = 'Invalid token.';
}