<?php
namespace App\Exception;

use Slim\Exception\HttpBadRequestException;

class BadHeaderException extends HttpBadRequestException
{
    protected $title = 'Bad header in the request.';
}