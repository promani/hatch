<?php
namespace App\Exception;

use Slim\Exception\HttpUnauthorizedException;

class PermissionDeniedException extends HttpUnauthorizedException
{
    protected $title = 'Permission Denied.';
}