<?php
namespace App\Exception;

use Slim\Exception\HttpUnauthorizedException;

class ExpiredTokenException extends HttpUnauthorizedException
{
    protected $title = 'Expired token.';
}