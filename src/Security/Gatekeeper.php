<?php
namespace App\Security;

use App\Exception\BadHeaderException;
use App\Exception\ExpiredTokenException;
use App\Exception\InvalidTokenException;
use App\Exception\MissingHeaderException;
use App\Exception\UnknownIssuerException;
use App\Factory\IssuerFactory;
use Lcobucci\JWT\Parser;
use Lcobucci\JWT\Signer\Key;
use Psr\Http\Message\ServerRequestInterface as Request;

class Gatekeeper
{
    private const USER_ID_KEY = 'client_id';

    /**
     * @throws ExpiredTokenException|InvalidTokenException|MissingHeaderException|BadHeaderException|UnknownIssuerException
     */
    public function check(Request $request): Request
    {
        $jwt = self::extract($request);
        try {
            $token = (new Parser())->parse($jwt);
        } catch (\Exception $exception) {
            throw new InvalidTokenException($request, $exception->getMessage());
        }

        $iss = $token->getClaim('iss');
        $factory = new IssuerFactory();
        $issuer = $factory->create($iss);

        if ($token->verify($issuer->getSigner(), new Key($issuer->getPublic()))) {
            $key = self::USER_ID_KEY;
            if ($token->hasClaim($key)) {
                return $request->withHeader('From', $token->getClaim($key).'@'.$token->getClaim('iss'));
            }

            throw new InvalidTokenException($request, sprintf("Your JWT must have an identifier for user in %s key", $key));
        }

        if ($token->isExpired()) {
            throw new ExpiredTokenException($request);
        }

        throw new InvalidTokenException($request);
    }

    /** @throws MissingHeaderException|BadHeaderException */
    private static function extract(Request $request): string
    {
        if (!$request->hasHeader('Authorization')) {
            throw new MissingHeaderException($request, sprintf("Your request needs %s header", $_ENV['HEADER']));
        }

        $jwt = $request->getHeaderLine('Authorization');

        $parts = explode(' ', $jwt);

        if (2 !== count($parts) && 0 === strcasecmp($parts[0], 'Bearer')) {
            throw new BadHeaderException($request, 'Your header should start with Bearer.');
        }

        return $parts[1];
    }

}
