<?php

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use Slim\Factory\AppFactory;
use App\Security\Gatekeeper;
use Symfony\Component\HttpClient\HttpClient;

require __DIR__ . '/../config/bootstrap.php';

$app = AppFactory::create();

$debug = $_ENV['APP_ENV'] !== 'prod';

$app->addErrorMiddleware($debug, $debug, $debug);

$app
    ->any('/{resource}[/{route:.*}]', static function (Request $request, Response $response, $args) use ($resources) {
        if (array_key_exists($args['resource'], $resources)) {
            $url = $resources[$args['resource']].$args['route'];
            $httpClient = HttpClient::create();
            $result = $httpClient->request($request->getMethod(), $url,
                [
                    'headers' => $request->getHeaders(),
                    'body' => [$request->getBody()]
                ]
            );
            $response->getBody()->write($result->getContent());
            return $response;
        }

        throw new \App\Exception\UnknownResourceException('Unknown resource '.$args['resource']);
    })
    ->add(static function (Request $request, RequestHandler $handler) {
        $guard = new Gatekeeper();
        if ($request = $guard->check($request)) {
            return $handler->handle($request);
        }
        throw new \App\Exception\PermissionDeniedException($request);
    });

$app->run();
